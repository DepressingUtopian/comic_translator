# Comic Translator
## Deploy
>  https://depressingutopian.gitlab.io/comic_translator/
## Макет
> _https://www.figma.com/file/vAeVaTfm3JBULKcoLptGve/ComicTranslator?node-id=0%3A1_
## Как запустить???
> * git clone https://gitlab.com/DepressingUtopian/comic_translator
> * npm install
> * npm run start
> * Перейти на http://localhost:3000

## Структура проекта
* **/assets** - для различных глобальных статических файлов (css, less, sass, images, icons, fonts), которые могут быть импортированы в бандл.
    * **/fonts** - файлы шрифтов
    * **/images** - картинки и иконки
    * **/styles** - стили
* **/components** - компоненты, имеющие некоторый функциональный смысл для пользователя
* **/containers** - совокупность компонентов
* **/hooks** - для хуков
    * **/ocr**
* **/pages** - компоненты страниц, зависят напрямую от URL, рендерятся из роутера. В страницу можно помещать контейнеры.
* **index.js** - точка входа

## Правила коммитов и работы в репозитории
1. Перед началом работы перейти в **master** (_git checkout master_) и сделать (_git pull master_).
2. Если ветки для выполнения задачи нет, сделать (_git checkout -b <название ветки>_), где название ветки это номер задачи в **gitlab issues**.
3. Коммиты называть так (_git commit -m "<название ветки> сообщение"_)
4. Перед **merge request** сделать (_git rebase master_), и пройти процесс rebase, подробнее (_https://www.atlassian.com/ru/git/tutorials/rewriting-history/git-rebase_)
5. После rebase и исправления всех конфликтов можете желать **merge request**, и писать что нужен код ревью!
6. Ветки называть так: <номер задачи>-описание задачи, например: 29-popup
7. Если накосячили писать telegram: @DepressingUtopian!

