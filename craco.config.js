const CopyPlugin = require('copy-webpack-plugin');
const { addBeforeLoader, loaderByName } = require('@craco/craco');

module.exports = {
  webpack: {
    plugins: [
      // Copy `unrar.wasm` to `assets/` folder of the output dir.
      new CopyPlugin({
        patterns:
          [{ from: 'node_modules/node-unrar-js/esm/js/*.wasm', to: 'assets/[name].[ext]' },
          { from: 'node_modules/libarchive.js/dist/wasm-gen/*wasm', to: 'assets/[name].[ext]' }],
      }),
    ],
    // configure: (webpackConfig) => {
    //   const wasmExtensionRegExp = /\.wasm$/;
    //   webpackConfig.resolve.extensions.push('.wasm');

    //   webpackConfig.module.rules.forEach((rule) => {
    //     (rule.oneOf || []).forEach((oneOf) => {
    //       if (oneOf.loader && oneOf.loader.indexOf('file-loader') >= 0) {
    //         oneOf.exclude.push(wasmExtensionRegExp);
    //       }
    //     });
    //   });

    //   const wasmLoader = {
    //     test: /\.wasm$/,
    //     exclude: /node_modules/,
    //     loaders: ['wasm-loader'],
    //   };

    //   addBeforeLoader(webpackConfig, loaderByName('file-loader'), wasmLoader);

    //   return webpackConfig;
    // },
  },
};
