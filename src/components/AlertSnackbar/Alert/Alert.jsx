import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import ReportProblemOutlinedIcon from '@material-ui/icons/ReportProblemOutlined';
import ErrorOutlineOutlinedIcon from '@material-ui/icons/ErrorOutlineOutlined';
import CheckBoxOutlinedIcon from '@material-ui/icons/CheckBoxOutlined';
import EcoOutlinedIcon from '@material-ui/icons/EcoOutlined';
import Typography from '@material-ui/core/Typography';
import useAlertStyles from './AlertStyles';

const Alert = ({ message, severity, onClose }) => {
  const getAlertIcon = (_severity) => {
    switch (_severity) {
      case 'error':
        return <ErrorOutlineOutlinedIcon fontSize="small" />;
      case 'warning':
        return <ReportProblemOutlinedIcon fontSize="small" />;
      case 'success':
        return <CheckBoxOutlinedIcon fontSize="small" />;
      default:
        return <EcoOutlinedIcon fontSize="small" />;
    }
  };
  return (
    <>
      {getAlertIcon(severity)}
      <Typography variant="caption" component="div">
        {message}
      </Typography>
      <IconButton size="small" aria-label="close" color="inherit" onClick={onClose}>
        <CloseIcon fontSize="small" />
      </IconButton>
    </>
  );
};

export default Alert;
