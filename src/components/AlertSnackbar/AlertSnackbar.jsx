import React, { useEffect } from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from './Alert/Alert';
import useAlertSnackbarStyles from './AlertSnackbarStyles';

const AlertSnackbar = ({
  message, isOpen, severity = 'default', onAlertClose,
}) => {
  const [open, setOpen] = React.useState(false);
  const classes = useAlertSnackbarStyles();
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    onAlertClose && onAlertClose();
    setOpen(false);
  };

  useEffect(() => {
    if (message) {
      setOpen(isOpen);
    }
  }, [message, isOpen]);

  const getSeverityStyle = (_severity) => {
    switch (_severity) {
      case 'error':
        return classes.error;
      case 'warning':
        return classes.warning;
      case 'success':
        return classes.success;
      case 'info':
        return classes.info;
      default:
        return classes.default;
    }
  };

  return (
    <Snackbar
      className={`${classes.alertSnackbar} ${getSeverityStyle(severity)}`}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right',
      }}
      open={open}
      onClose={handleClose}
      autoHideDuration={5000}
      action={(
        <>
          <Alert message={message} severity={severity} onClose={handleClose} />
        </>
        )}
    />
  );
};

export default AlertSnackbar;
