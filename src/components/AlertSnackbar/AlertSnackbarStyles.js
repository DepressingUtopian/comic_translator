import { makeStyles } from '@material-ui/core/styles';

const useAlertSnackbarStyles = makeStyles((theme) => ({
  alertSnackbar: {
    '& .MuiSnackbarContent-root': {
      backgroundColor: 'unset',
    },
    '& .MuiSnackbarContent-action': {
      marginLeft: 'unset',
      paddingLeft: 'unset',
      display: 'flex',
      justifyContent: 'space-between',
      '& .MuiTypography-root': {
        marginLeft: '14px',
      },
    },
  },
  error: {
    background: 'crimson',
  },
  warning: {
    background: 'orangered',
  },
  success: {
    background: 'mediumseagreen',
  },
  info: {
    background: 'dodgerblue',
  },
  default: {
    background: 'darkslategray',
  },

}));

export default useAlertSnackbarStyles;
