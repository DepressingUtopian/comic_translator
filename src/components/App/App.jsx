import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
  Route,
} from 'react-router-dom';
import HomePage from '../../pages/HomePage/HomePage';
import ReaderPage from '../../pages/ReaderPage/ReaderPage';
import PageContentWrapper from '../../containers/PageContentWrapper/PageContentWrapper';

export default function App(props) {
  return (
    <Router className="App" basename={process.env.PUBLIC_URL}>
      <Switch>
        <Route path="/home">
          <PageContentWrapper hideFooter hideMenu>
            <HomePage />
          </PageContentWrapper>
        </Route>
        <Route path="/reader">
          <PageContentWrapper hideFooter>
            <ReaderPage />
          </PageContentWrapper>
        </Route>
        <Redirect from="/" to="home" />
      </Switch>
    </Router>
  );
}
