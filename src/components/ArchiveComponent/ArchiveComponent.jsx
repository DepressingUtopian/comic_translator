import React, { useEffect } from 'react';
import { Archive } from 'libarchive.js/main';
import { useHistory } from 'react-router';
import { DropzoneAreaBase } from 'material-ui-dropzone';
import { createExtractorFromData } from 'node-unrar-js/esm';
import archiveStyle from './ArchiveComponentStyle';
import useDatabase from '../../hooks/useDatabase';

Archive.init({
  workerUrl: 'libarchive.js/dist/worker-bundle.js',
});

const allowedFormats = ['png', 'jpeg', 'jpg'];

function ArchiveComponent(props) {
  const classes = archiveStyle();
  const dataBase = useDatabase();
  const history = useHistory();
  useEffect(() => {
    dataBase.onsuccess = () => {
      const transaction = dataBase.result.transaction(['comic'], 'readwrite');
      const objectStore = transaction.objectStore('comic');
      objectStore.clear();
    };
  });
  // В текущий момент реализация работает для папок
  // c любой вложенностью, подбирает все картинки из поддиректорий etc.
  function objectDiving(obj) {
    const reducer = (accumulator, value) => {
      if (Object.values(value).length !== 0) {
        return Object.values(value).reduce(reducer, []);
      }
      const pictureValidation = value.name.substring(value.name.indexOf('.') + 1);

      if (pictureValidation === 'jpg' || pictureValidation === 'png') {
        return [...accumulator, value];
      }
      return accumulator;
    };
    return Object.values(obj).reduce(reducer, []);
  }
  function addFilesToDb(data) {
    const transaction = dataBase.result.transaction(['comic'], 'readwrite');
    const objectStore = transaction.objectStore('comic');
    console.log(data);
    data.forEach((el) => {
      const request = objectStore.add(el);
      request.onerror = (e) => console.error(request.error.message, e);
    });
    transaction.oncomplete = () => {
      history.push('/reader');
    };
  }
  // eslint-disable-next-line consistent-return
  const compareFiles = (a, b) => {
    if (a.fileHeader.name < b.fileHeader.name) {
      return -1;
    }
    if (a.fileHeader.name > b.fileHeader.name) {
      return 1;
    }
    return 0;
  };
  const getFormat = (name) => name.fileHeader.name.split('.')[1];
  const onArchiveLoad = async (e) => {
    const path = e[0].file;
    const pathSplitted = path.name.split('.');
    localStorage.setItem('archiveName', pathSplitted[0]);
    if (pathSplitted[1] === 'cbr' || pathSplitted[1] === 'CBR') {
      const archive = await path.arrayBuffer();
      const uint8 = new Uint8Array(archive);
      const wasmBinary = await (await fetch('./assets/unrar.wasm', { credentials: 'same-origin' })).arrayBuffer();
      const extractor = await createExtractorFromData({ wasmBinary, data: uint8 });
      const extracted = await extractor.extract();
      const files = [...extracted.files].filter((elem) => allowedFormats
        .indexOf(getFormat(elem)) >= 0);
      files.sort(compareFiles);
      const blobedFiles = files.map((fileBuffer) => {
        const a = new Blob([fileBuffer.extraction], { type: `image/${getFormat(fileBuffer) === 'png' ? 'png' : 'jpeg'}` });
        return a;
      });
      addFilesToDb(blobedFiles);
    } else {
      const archive = await Archive.open(path);
      const obj = await archive.extractFiles();
      const resultOfDiving = objectDiving(obj);
      addFilesToDb(resultOfDiving);
    }
  };

  return (
    <div className={classes.dragndrop}>
      <DropzoneAreaBase
        className={classes.dragndropZone}
        disableRejectionFeedback
        acceptedFiles={['.cbz', '.CBZ', '.cbr', '.CBR']}
        maxFileSize={5000000000}
        filesLimit={1}
        dropzoneText="Перетащи сюда архив или кликни"
        onAdd={onArchiveLoad}
      />
    </div>
  );
}
export default ArchiveComponent;
