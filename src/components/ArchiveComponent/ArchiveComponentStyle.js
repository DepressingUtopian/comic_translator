import { makeStyles } from '@material-ui/core/styles';

const archiveStyle = makeStyles({

  dragndrop: {
    color: 'black',
    maxWidth: '32vw',
    padding: '0 4px',
    '&  .MuiDropzoneArea-text': {
      fontFamily: 'RobotoSlab',
      fontSize: '1.7rem',
    },
  },
});
export default archiveStyle;
