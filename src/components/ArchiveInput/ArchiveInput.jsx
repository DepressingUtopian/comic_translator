import React from 'react';
import { DropzoneAreaBase } from 'material-ui-dropzone';

function ArchiveInput({ onArchiveLoad }) {
  return (
    <DropzoneAreaBase
      disableRejectionFeedback
      acceptedFiles={['.cbz', '.CBZ']}
      maxFileSize={500000000}
      filesLimit={1}
      dropzoneText="Перетащите сюда архив с комиксом или кликните что бы выбрать его из контекстного меню"
      onAdd={onArchiveLoad}
    />

  );
}

export default ArchiveInput;
