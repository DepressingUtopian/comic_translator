import React, { useEffect, useRef } from 'react';

export default function Canvas(props) {
  const canvasRef = useRef(null);
  const { setCanvas, theCSSImageWidth, theCSSImageHeight } = props;

  function onMouseDownHandler(event) {
    props.onMouseDown(event);
  }
  function mouseMoveHandler(event) {
    props.onMouseMove(event);
  }
  function mouseUpHandler(event) {
    props.onMouseUp(event);
  }
  function onMouseOutHandler(e) {
    props.onMouseOut(e);
  }

  useEffect(() => {
    setCanvas(canvasRef.current);
  }, []);

  return (
    // eslint-disable-next-line jsx-a11y/mouse-events-have-key-events
    <canvas
      width={theCSSImageWidth}
      height={theCSSImageHeight}
      ref={canvasRef}
      onMouseMove={mouseMoveHandler}
      onMouseUp={mouseUpHandler}
      onMouseDown={onMouseDownHandler}
      onMouseOut={onMouseOutHandler}
    />
  );
}
