/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect, useState } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

function CircularProgressWithLabel(props) {
  return (
    <Box position="relative" display="inline-flex">
      <CircularProgress variant="determinate" {...props} />
      <Box
        top={0}
        left={0}
        bottom={0}
        right={0}
        position="absolute"
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        <Typography variant="caption" component="div" color="inherit">
          {`${Math.round(props.value)}%`}
        </Typography>
      </Box>
    </Box>
  );
}

export default function CircularStatic({ value }) {
  const [progress, setProgress] = useState(value);
  const normalizeProgress = (_value) => (_value * 100 >= 100 ? 100 : _value * 100);
  useEffect(() => {
    const normProg = normalizeProgress(value);
    setProgress(normProg);
  }, [value]);

  return <CircularProgressWithLabel value={progress} />;
}
