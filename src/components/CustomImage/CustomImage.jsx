/* eslint-disable max-len */
import React, { useEffect, useRef } from 'react';
// import useResizeObserver from '@react-hook/resize-observer';

const CustomImage = (props) => {
  const imageRef = useRef(null);
  const {
    alt,
    imageSource,
    setImage,
    setCSSImageWidth,
    setCSSImageHeight,
  } = props;
  const observer = useRef(new ResizeObserver((entries) => {
    if (imageRef) {
      if (entries[0].contentRect.height !== 0 && entries[0].contentRect.width !== 0) {
        setCSSImageHeight(entries[0].contentRect.height);
        setCSSImageWidth(entries[0].contentRect.width);
      }
    }
  }));
  function handleImageLoad() {
    setImage(imageRef.current);
  }
  useEffect(() => {
    // const resizeObserver = new ResizeObserver((entries) => {
    //   console.log(entries[0].contentRect.width);
    //   console.log(entries[0].contentRect.height);
    // });
    observer.current.observe(imageRef.current);
  }, [imageRef.current]);

  // useEffect(() => {
  //   if (imageRef) {
  //     const theCSSImageHeight = window.getComputedStyle(imageRef.current, null).getPropertyValue('height');
  //     const theCSSImageWidth = window.getComputedStyle(imageRef.current, null).getPropertyValue('width');
  //     setCSSImageWidth(theCSSImageWidth);
  //     setCSSImageHeight(theCSSImageHeight);
  //   }
  // });

  return (
    <img
      ref={imageRef}
      alt={alt}
      src={imageSource}
      onLoad={handleImageLoad}
    />
  );
};

export default CustomImage;
