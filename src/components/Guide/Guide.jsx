/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

import {
  Link, List, ListItem, ListItemIcon, SvgIcon, Box,
} from '@material-ui/core';

import MenuBookIcon from '@material-ui/icons/MenuBook';
import CropFreeIcon from '@material-ui/icons/CropFree';
import CloseIcon from '@material-ui/icons/Close';
import LocationSearchingIcon from '@material-ui/icons/LocationSearching';
import FlipToBackIcon from '@material-ui/icons/FlipToBack';
import Typography from '@material-ui/core/Typography';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';

import StepButton from '@material-ui/core/StepButton';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

import anim1 from '../../assets/images/guide/anim1.gif';
import anim2 from '../../assets/images/guide/anim2.gif';
import anim3 from '../../assets/images/guide/anim3.gif';
import anim4 from '../../assets/images/guide/anim4.gif';
import anim5 from '../../assets/images/guide/anim5.gif';

import pagination from '../../assets/images/guide/pagination.PNG';

import { ReactComponent as keyW } from '../../assets/icons/keyboard/w.svg';
import { ReactComponent as keyA } from '../../assets/icons/keyboard/a.svg';
import { ReactComponent as keyS } from '../../assets/icons/keyboard/s.svg';
import { ReactComponent as keyD } from '../../assets/icons/keyboard/d.svg';
import { ReactComponent as keyQ } from '../../assets/icons/keyboard/q.svg';
import { ReactComponent as keyE } from '../../assets/icons/keyboard/e.svg';

import GuideItem from '../GuideItem/GuideItem';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& a': {
      margin: '0 2pt',
    },
  },
  button: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  controlButtons: {
    display: 'flex',
    justifyContent: 'space-between',
  },
}));

const horList = {
  display: 'flex',
  flexDirection: 'row',
  padding: 0,
};

const typWrapper = {
  display: 'flex',
  alignItems: 'center',
};

function getSteps() {
  return ['Источник данных', 'Взаимодействие с комиксом', 'Выделение фрагмента для OCR', 'Окно с результатами OCR', 'Завершение работы'];
}

function getStepContent(step) {
  switch (step) {
    case 0:
      return (
        <GuideItem
          imageSrc={anim1}
          header="Источник данных"
          description={(
            <Typography>
              Выберите или перетащите архив в
              формате
              <Link href="https://ru.wikipedia.org/wiki/Comic_Book_Archive">
                cbr
              </Link>
              или
              <Link href="https://ru.wikipedia.org/wiki/Comic_Book_Archive">
                cbz
              </Link>
              !
            </Typography>
          )}
        />
      );
    case 1:
      return (
        <GuideItem
          imageSrc={anim2}
          header="Источник данных"
          description={(
            <>
              <Typography>
                На странице просмотра комикса можно (панель справа):
              </Typography>
              <List style={horList}>
                <ListItem>
                  <ListItemIcon>
                    <LocationSearchingIcon />
                  </ListItemIcon>
                  Масштабировать
                </ListItem>
                <ListItem>
                  <ListItemIcon>
                    <CropFreeIcon />
                  </ListItemIcon>
                  Выделять фрагмент для распознования
                </ListItem>
                <ListItem>
                  <ListItemIcon>
                    <MenuBookIcon />
                  </ListItemIcon>
                  Двухстраничный режим
                </ListItem>
                <ListItem>
                  <ListItemIcon>
                    <FlipToBackIcon />
                  </ListItemIcon>
                  Скрывать окно с результатом распознования
                </ListItem>
                <ListItem>
                  <ListItemIcon>
                    <CloseIcon />
                  </ListItemIcon>
                  Выбрать другой архив с комиксом
                </ListItem>
              </List>
              <Typography style={typWrapper}>
                Управление в режиме масштабирования
                <LocationSearchingIcon fontSize="inherit" />
                :
              </Typography>
              <List style={horList}>
                <ListItem>
                  <ListItemIcon>
                    <SvgIcon component={keyW} viewBox="0 0 377.343 377.343" fontSize="large" />
                  </ListItemIcon>
                  Сдвиг вверх
                </ListItem>
                <ListItem>
                  <ListItemIcon>
                    <SvgIcon component={keyA} viewBox="0 0 377.343 377.343" fontSize="large" />
                  </ListItemIcon>
                  Сдвиг влево
                </ListItem>
                <ListItem>
                  <ListItemIcon>
                    <SvgIcon component={keyS} viewBox="0 0 377.343 377.343" fontSize="large" />
                  </ListItemIcon>
                  Сдвиг вниз
                </ListItem>
                <ListItem>
                  <ListItemIcon>
                    <SvgIcon component={keyD} viewBox="0 0 407.601 407.601" fontSize="large" />
                  </ListItemIcon>
                  Сдвиг вправо
                </ListItem>
                <ListItem>
                  <ListItemIcon>
                    <SvgIcon component={keyQ} viewBox="0 0 407.601 407.601" fontSize="large" />
                  </ListItemIcon>
                  + масштаб
                </ListItem>
                <ListItem>
                  <ListItemIcon>
                    <SvgIcon component={keyE} viewBox="0 0 407.601 407.601" fontSize="large" />
                  </ListItemIcon>
                  - масштаб
                </ListItem>
              </List>
              <Typography>
                Переключение страниц (панель снизу):
              </Typography>
              <Box>
                <img src={pagination} />
              </Box>
            </>
          )}
        />
      );
    case 2:
      return (
        <GuideItem
          imageSrc={anim3}
          header="Источник данных"
          description={(
            <Typography style={typWrapper}>
              В режиме выделения
              <CropFreeIcon fontSize="inherit" />
              , выделите необходимый участок, после этого начнется процесс распознования.
            </Typography>
          )}
        />
      );
    case 3:
      return (
        <GuideItem
          imageSrc={anim4}
          header="Источник данных"
          description={(
            <Typography>
              В окне с результатом распознования,
              можно отредактировать текст, перевести его,
              узнать значение каждого отдельного слова.
            </Typography>
          )}
        />
      );
    case 4:
      return (
        <GuideItem
          imageSrc={anim5}
          header="Источник данных"
          description={(
            <Typography style={typWrapper}>
              Чтобы закрыть страницу с комиксом и выбрать другой архив, нажмите:
              <CloseIcon fontSize="inherit" />
            </Typography>
          )}
        />
      );
    default:
      return 'Неизвестный шаг';
  }
}

const Guide = () => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [completed, setCompleted] = React.useState({});
  const steps = getSteps();

  const totalSteps = () => steps.length;

  const completedSteps = () => Object.keys(completed).length;

  const isLastStep = () => activeStep === totalSteps() - 1;

  const allStepsCompleted = () => completedSteps() === totalSteps();

  const handleNext = () => {
    const newActiveStep = isLastStep() && !allStepsCompleted()
      ? steps.findIndex((step, i) => !(i in completed))
      : activeStep + 1;
    setActiveStep(newActiveStep);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStep = (step) => () => {
    setActiveStep(step);
  };

  const handleReset = () => {
    setActiveStep(0);
    setCompleted({});
  };

  return (
    <div className={classes.root}>
      <Stepper nonLinear activeStep={activeStep}>
        {steps.map((label, index) => (
          <Step key={label}>
            <StepButton onClick={handleStep(index)} completed={completed[index]}>
              {label}
            </StepButton>
          </Step>
        ))}
      </Stepper>
      <div>
        {allStepsCompleted() ? (
          <div>
            <Button onClick={handleReset}>Reset</Button>
          </div>
        ) : (
          <div>
            {getStepContent(activeStep)}
            <div className={classes.controlButtons}>
              <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button}>
                Назад
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={handleNext}
                className={classes.button}
              >
                Далее
              </Button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Guide;
