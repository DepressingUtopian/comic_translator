import React from 'react';

import CardMedia from '@material-ui/core/CardMedia';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  item: {
    maxHeight: '52vh',
    marginBottom: '10px',
    overflowY: 'scroll',
  },
  paper: {
    padding: '6px 16px',
  },
  secondaryTail: {
    backgroundColor: theme.palette.secondary.main,
  },
  media: {
    height: '100vh',
  },
  mediaWrapper: {
    maxHeight: '42vh',
    overflowY: 'scroll',
    marginBottom: '10px',
  },
}));

const GuideItem = ({
  imageSrc, header, description,
}) => {
  const classes = useStyles();

  return (
    <div className={classes.item}>
      <div>
        <Paper elevation={3} className={classes.paper}>
          <Typography variant="h6" component="h1">
            {header}
          </Typography>
          <Box className={classes.mediaWrapper}>
            <CardMedia
              className={classes.media}
              image={imageSrc}
            />
          </Box>
          {description}
        </Paper>
      </div>
    </div>
  );
};

export default GuideItem;
