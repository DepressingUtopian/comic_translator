/* eslint-disable react/prop-types */
import React, { useState, useRef } from 'react';

export default function ImageHidden(props) {
  // eslint-disable-next-line no-unused-vars
  const [isLoaded, setState] = useState(false);

  const {
    alt, imageSource, setImage,
  } = props;
  const imageRef = useRef(null);

  function handleImageLoad() {
    setState(true);
    setImage(imageRef.current);
  }

  return (
    <div>
      <img
        style={{ display: 'none' }}
        ref={imageRef}
        alt={alt}
        src={imageSource}
        onLoad={handleImageLoad}
      />
    </div>
  );
}
