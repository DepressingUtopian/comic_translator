import React, { useState, useEffect, useRef } from 'react';
import AlertSnackbar from '../AlertSnackbar/AlertSnackbar';
import Canvas from '../Canvas/Canvas';
import ImageHidden from '../ImageHidden/ImageHidden';
import TempCanvas from '../TempCanvas/TempCanvas';

export default function ImageWithSelecting(props) {
  const [imageRef, setImage] = useState(null);
  const [canvasRef, setCanvas] = useState(null);

  const [tempCanvasRef, setTempCanvas] = useState(null);

  const [keyDownX, setKeyDownX] = useState(null);
  const [keyDownY, setKeyDownY] = useState(null);

  const [isDown, setIsDown] = useState(false);
  const imageData = useRef(null);
  const imageDataWithoutFilter = useRef(null);

  const [alertMessage, setAlertMessage] = useState('');
  const [isOpenAlert, setIsOpenAlert] = useState(false);
  // const [croppedImageBase64Url, setUrl] = useState(''); // base64Url выделенного изображения

  const {
    source,
    theCSSImageWidth,
    theCSSImageHeight,
    croppedImageBase64Url,
    setUrl,
  } = props;

  function onAlertClose() {
    setIsOpenAlert(false);
  }

  function mouseMoveHandler(e) {
    const currentX = e.nativeEvent.offsetX;
    const currentY = e.nativeEvent.offsetY;

    if (isDown) {
      const width = currentX - keyDownX;
      const height = currentY - keyDownY;

      const ctx = canvasRef.getContext('2d');
      ctx.putImageData(imageData.current, 0, 0);
      ctx.strokeRect(keyDownX, keyDownY, width, height);
      ctx.putImageData(imageDataWithoutFilter.current, 0, 0, keyDownX, keyDownY, width, height);
    }
  }

  // Положили base64 строку в состояние компонента
  function mouseUpHandler(e) {
    // очищает канвас
    const ctx2 = canvasRef.getContext('2d');
    canvasRef.width = canvasRef.width; // так нужно для очистки канваса
    ctx2.putImageData(imageData.current, 0, 0);

    const keyUpX = e.nativeEvent.offsetX;
    const keyUpY = e.nativeEvent.offsetY;

    if (isDown) {
      setIsDown(false);

      const hCoof = imageRef.naturalHeight / canvasRef.height;
      const wCoof = imageRef.naturalWidth / canvasRef.width;

      const x0 = keyDownX * wCoof;
      const y0 = keyDownY * hCoof;

      // эти десятки из-за кривого выделелния области. См функцию calculateWidthAndHeight
      const x1 = (keyUpX) * wCoof;
      const y1 = (keyUpY) * hCoof;

      const newWidth = x1 - x0;
      const newHeigh = y1 - y0;
      if (newWidth < 0 && newHeigh < 0) {
        tempCanvasRef.width = Math.abs(newWidth);
        tempCanvasRef.height = Math.abs(newHeigh);

        const ctx = tempCanvasRef.getContext('2d');
        ctx.drawImage(imageRef,
          x1, y1, newWidth, newHeigh, 0, 0, tempCanvasRef.width, tempCanvasRef.height);
      }

      tempCanvasRef.width = Math.abs(newWidth);
      tempCanvasRef.height = Math.abs(newHeigh);

      const ctx = tempCanvasRef.getContext('2d');
      ctx.drawImage(imageRef,
        x0, y0, newWidth, newHeigh, 0, 0, tempCanvasRef.width, tempCanvasRef.height);
      const url = tempCanvasRef.toDataURL('image/png', 1.0); // Длина base64 строки в хроме  должна быть <=2mb. Это ограничениие хрома

      // Положили base64 строку в состояние компонента
      setUrl(url);
    }
  }

  function mouseDownHandler(e) {
    setIsDown(true);
    setKeyDownX(e.nativeEvent.offsetX);
    setKeyDownY(e.nativeEvent.offsetY);
  }

  function onMouseOut(e) {
    if (isDown) {
      const ctx2 = canvasRef.getContext('2d');
      canvasRef.width = canvasRef.width; // так нужно для очистки канваса
      ctx2.putImageData(imageData.current, 0, 0);
      setAlertMessage('Курсор покинул область без выделеления!');
      setIsOpenAlert(true);
      setIsDown(false);
    }
  }

  useEffect(() => {
    if (canvasRef && imageRef) {
      const ctx = canvasRef.getContext('2d');

      ctx.drawImage(imageRef, 0, 0, canvasRef.width, canvasRef.height);
      imageData.current = ctx.getImageData(0, 0, canvasRef.width, canvasRef.height);
      imageDataWithoutFilter.current = ctx.getImageData(0, 0, canvasRef.width, canvasRef.height);
      const pixels = imageData.current.data;

      for (let i = 0; i < pixels.length; i += 4) {
        pixels[i] -= pixels[i] / 2;
        pixels[i + 1] -= pixels[i + 1] / 2;
        pixels[i + 2] -= pixels[i + 2] / 2;
        pixels[i + 3] = 255 * 1;
      }
      ctx.putImageData(imageData.current, 0, 0);
    }
  }, [canvasRef, imageRef]);

  return (

    <>
      <ImageHidden
        setImage={setImage}
        imageSource={source}
        alt="HidenImage"
      />
      <div style={{ position: 'relative' }}>
        <Canvas
          theCSSImageWidth={theCSSImageWidth}
          theCSSImageHeight={theCSSImageHeight}
          onMouseMove={mouseMoveHandler}
          onMouseUp={mouseUpHandler}
          onMouseDown={mouseDownHandler}
          onMouseOut={onMouseOut}
          setCanvas={setCanvas}
        />
      </div>
      <TempCanvas setTempCanvas={setTempCanvas} />
      <div>
        <AlertSnackbar message={alertMessage} isOpen={isOpenAlert} severity="info" onAlertClose={onAlertClose} />
      </div>

    </>

  );
}
