import React from 'react';
import Pagination from '@material-ui/lab/Pagination';
import styles from './paginationControlled.module.css';

export default function PaginationControlled(props) {
  function handleChange(event, value) {
    setSliderState(value);
  }

  const {
    isSelectedImage, imgLinks, setSliderState, slideNumber,
  } = props;
  return (
    <Pagination
      className={styles.MuiPaginationRoot}
      count={imgLinks.length}
      page={slideNumber}
      onChange={handleChange}
      shape="rounded"
      disabled={isSelectedImage}
    />
  );
}
