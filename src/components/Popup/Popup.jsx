import React, { useEffect, useMemo, useState } from 'react';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { Paper, CircularProgress, Typography } from '@material-ui/core';
import Draggable from 'react-draggable';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary';
import CloseIcon from '@material-ui/icons/Close';

import usePopupStyles from './PopupStyles';
import EditTab from './Tabs/EditTab/EditTab';
import TranslateTab from './Tabs/TranslateTab/TranslateTab';
import TrainTab from './Tabs/TrainTab/TrainTab';
import SwitchPad from './SwitchPad/SwitchPad';

const Popup = (props) => {
  const classes = usePopupStyles();
  const [open, setOpen] = useState(false);
  const [minimizated, setMinimizated] = useState(false);
  const [loading, setLoading] = React.useState(true);
  const {
    data, onUpdateExternalData, onDeleteFromExternalData, onError, onVisible,
  } = props;
  const [currentSelectedItem, setCurrentSelectedItem] = useState(null);
  const currentSelectedText = useMemo(() => (currentSelectedItem
    ? currentSelectedItem.text.replace(/\n|\r|\0|\r|\t/g, ' ') : ''), [currentSelectedItem]);
  const draggableRef = React.useRef(null);

  useEffect(() => {
    if (!currentSelectedItem || data.content.length === 0) {
      setCurrentSelectedItem(data?.content ? data.content[0] : null);
    }
  }, [data, data.content]);

  useEffect(() => setLoading(!(currentSelectedItem)), [currentSelectedItem]);

  useEffect(() => onVisible(!!currentSelectedItem), [currentSelectedItem, onVisible]);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const toggleMinimization = () => {
    setMinimizated(!minimizated);
    handleDrawerClose();
  };

  const onClickListItem = (item) => {
    setCurrentSelectedItem(item);
  };

  const onChangeEditTab = (value) => {
    const { text, ...rest } = currentSelectedItem;
    const newItem = { text: value, ...rest };
    setCurrentSelectedItem(newItem);
    onUpdateExternalData({ text: value, ...rest });
  };

  const onClickDeleteButton = (event, item) => {
    if (data.content.length === 1) {
      setCurrentSelectedItem(null);
      setCurrentTabIndex(0);
    }
    onDeleteFromExternalData(item);
    event.stopPropagation();
  };

  // Вкладки которые можно переключать
  const tabs = [
    {
      header: loading ? 'Загрузка...' : 'Редактирование',
      component: <EditTab text={currentSelectedText} onChange={onChangeEditTab} />,
    },
    {
      header: 'Перевод',
      component: <TranslateTab text={currentSelectedText} />,
    },
    {
      header: 'Обучение',
      component: <TrainTab text={currentSelectedText} />,
    },
  ];

  const [currentTabIndex, setCurrentTabIndex] = useState(0);

  const onChangeTad = (index) => {
    setCurrentTabIndex(index);
  };
  return (
    <Draggable bounds="parent" nodeRef={draggableRef} cancel=".MuiTextField-root, .MuiButtonBase-root, .MuiTypography-root">
      <Paper className={`${classes.main} ${!currentSelectedItem ? classes.hide : ''} ${open ? classes.mainShift : ''}`} ref={draggableRef}>
        <div className={classes.root}>
          <CssBaseline />
          <AppBar
            position="relative"
            className={`${classes.appBar} ${open ? classes.appBarShift : ''}`}
          >
            <Toolbar>
              <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
                edge="start"
                className={`${classes.menuButton} ${(open || minimizated || loading) ? classes.hide : ''}`}
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" noWrap>
                {tabs[currentTabIndex].header}
              </Typography>
              {
                loading ? <CircularProgress color="inherit" className={classes.loadingIcon} />
                  : (
                    <IconButton
                      color="inherit"
                      className={classes.minimizationButton}
                      onClick={() => toggleMinimization()}
                    >
                      {minimizated ? <KeyboardArrowDownIcon fontSize="large" /> : <KeyboardArrowUpIcon fontSize="large" />}
                    </IconButton>
                  )
              }
            </Toolbar>
          </AppBar>
          <Drawer
            className={`${minimizated ? classes.hide : classes.drawer}`}
            variant="persistent"
            anchor="left"
            open={open}
            transitionDuration={0}
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            <div className={classes.drawerHeader}>
              <IconButton onClick={handleDrawerClose}>
                <ChevronLeftIcon />
              </IconButton>
            </div>
            <Divider />
            <List>
              {data?.content?.map((item, index) => {
                const header = item.header
                  ? item.header : item?.text?.substring(0, 20);
                return (
                  <ListItem
                    button
                    // eslint-disable-next-line react/no-array-index-key
                    key={index}
                    onClick={() => onClickListItem(item)}
                    selected={item === currentSelectedItem}
                  >
                    <ListItemIcon>
                      <LocalLibraryIcon />
                    </ListItemIcon>
                    <ListItemText primary={header} />
                    <IconButton onClick={(event) => onClickDeleteButton(event, item)}>
                      <CloseIcon />
                    </IconButton>
                  </ListItem>
                );
              })}
            </List>
            <Divider />
          </Drawer>
          <main
            className={`${minimizated || loading ? classes.hide : classes.content} ${open ? classes.contentShift : ''}`}
          >
            {tabs[currentTabIndex].component}
            <SwitchPad countVariants={tabs.length} onChange={onChangeTad} />
          </main>
        </div>
      </Paper>
    </Draggable>
  );
};

export default Popup;
