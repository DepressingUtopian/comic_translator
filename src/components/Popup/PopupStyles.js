import { makeStyles } from '@material-ui/core/styles';

const drawerWidth = 300;
const usePopupStyles = makeStyles((theme) => ({
  main: {
    position: 'absolute',
    width: '400px',
    zIndex: 999,
  },
  mainShift: {
    width: `${400 + drawerWidth}px`,
  },
  root: {
    display: 'flex',
    flexDirection: 'column',
    position: 'relative',
  },
  appBar: {
    background: '#333333',
    color: 'white',
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  minimizationButton: {
    marginLeft: 'auto',
    padding: '2px',
  },
  loadingIcon: {
    marginLeft: 'auto',
    padding: '2px',
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    transition: 'initial',
    '& .MuiTypography-displayBlock': {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    },
  },
  drawerPaper: {
    width: drawerWidth,
    position: 'absolute',
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: '24px 24px 10px 24px',
    marginLeft: 0,
    minHeight: '277px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  contentShift: {
    marginLeft: drawerWidth,
  },
  select: {
    '& .MuiInputBase-input': {
      color: 'white',
    },
    '& .MuiInputBase-input option': {
      color: 'black',
    },
    '&:before': {
      borderColor: 'white',
    },
    '&:after': {
      borderColor: 'white',
    },
  },
  icon: {
    fill: 'white',
  },
}));

export default usePopupStyles;
