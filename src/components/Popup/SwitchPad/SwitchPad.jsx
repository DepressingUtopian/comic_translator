import React, { useEffect } from 'react';
import { Link } from '@material-ui/core';

import useSwitchPadStyles from './SwitchPadStyles';

const SwitchPad = ({ countVariants, onChange }) => {
  const [currentHeaderIndex, setCurrentHeaderIndex] = React.useState(0);
  const classes = useSwitchPadStyles();

  const IncrementIndex = () => {
    setCurrentHeaderIndex(currentHeaderIndex + 1 >= countVariants ? 0 : currentHeaderIndex + 1);
  };

  const DecrementIndex = () => {
    setCurrentHeaderIndex(currentHeaderIndex <= 0 ? countVariants - 1 : currentHeaderIndex - 1);
  };

  useEffect(() => {
    onChange && onChange(currentHeaderIndex);
  }, [currentHeaderIndex]);

  return (
    <div className={classes.bottomControlPanel}>
      <Link onClick={IncrementIndex} className={classes.padButton}>
        {currentHeaderIndex === 0 && 'Перевод'}
        {currentHeaderIndex === 1 && 'Обучение'}
        {currentHeaderIndex === 2 && 'Редактирование'}

      </Link>
      <Link onClick={DecrementIndex} className={classes.padButton}>
        {currentHeaderIndex === 0 && 'Обучение'}
        {currentHeaderIndex === 1 && 'Редактирование'}
        {currentHeaderIndex === 2 && 'Перевод'}
      </Link>
    </div>
  );
};

export default SwitchPad;
