import { makeStyles } from '@material-ui/core/styles';

const useSwitchPadStyles = makeStyles((theme) => ({
  bottomControlPanel: {
    width: '100%',
    marginTop: '8px',
    display: 'flex',
    justifyContent: 'space-between',
  },
  padButton: {
    cursor: 'pointer',
  },
}));

export default useSwitchPadStyles;
