import React from 'react';
import { TextField } from '@material-ui/core';

import useEditTabStyles from './EditTabStyles';

const EditTab = ({ text, onChange }) => {
  const classes = useEditTabStyles();

  return (
    <TextField
      className={classes.TextField}
      multiline
      value={text}
      onChange={(event) => onChange && onChange(event.target.value)}
      variant="outlined"
      rows={10}
    />
  );
};

export default EditTab;
