import { makeStyles } from '@material-ui/core/styles';

const useEditTabStyles = makeStyles((theme) => ({
  TextField: {
    width: '100%',
  },
}));

export default useEditTabStyles;
