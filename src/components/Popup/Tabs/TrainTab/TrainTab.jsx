import React from 'react';
import { Container } from '@material-ui/core';

import useTrainAreaStyles from './TrainTabStyles';
import TrainWord from './TrainWord/TrainWord';

const сlearText = (text) => text && text.replace(/ +(?= )|-|\?|\.|\!|\,|\:|\;|\'|\"/g, '');

const TrainTab = ({ text }) => {
  const classes = useTrainAreaStyles();
  const array = text && сlearText(text).split(' ').map((element, index) => (
    <TrainWord
      // eslint-disable-next-line react/no-array-index-key
      key={index}
      value={element}
      descriptionText="Представляющий собой загадку. “Загадочная картинка”"
    />
  ));

  return (
    <Container className={classes.TrainTextField}>
      {array}
    </Container>
  );
};

export default TrainTab;
