import { makeStyles } from '@material-ui/core/styles';

const useTrainTabStyles = makeStyles((theme) => ({
  TrainTextField: {
    display: 'flex',
    flexFlow: 'wrap',
    alignContent: 'baseline',
    '& p': {
      display: 'inline-block',
      margin: '2px 4px',
    },
  },
}));

export default useTrainTabStyles;
