import React from 'react';
import Typography from '@material-ui/core/Typography';

import useTrainTooltipTitleStyles from './TrainTooltipTitleStyles';

const TrainTooltipTitle = ({ text, translatedText, descriptionText }) => {
  const classes = useTrainTooltipTitleStyles();

  return (
    <div>
      <Typography variant="subtitle1" color="textSecondary">Перевод</Typography>
      <div className={classes.tooltipHeader}>
        <Typography variant="h6" color="inherit">{translatedText}</Typography>
        <Typography variant="subtitle2" color="textSecondary">{text}</Typography>
      </div>

      <Typography>{descriptionText}</Typography>
      <Typography className={classes.link}>
        <a target="_blank" rel="noreferrer" href={`https://dictionary.cambridge.org/dictionary/english-russian/${text}`}>
          Узнать больше
        </a>

      </Typography>
    </div>
  );
};

export default TrainTooltipTitle;
