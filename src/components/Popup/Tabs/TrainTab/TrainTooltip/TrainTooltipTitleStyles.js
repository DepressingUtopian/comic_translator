import { makeStyles } from '@material-ui/core/styles';

const useTrainTooltipTitleStyles = makeStyles((theme) => ({
  tooltipHeader: {
    padding: '0 2px',
  },
  description: {
    color: '#000000',
  },
  link: {
    fontWeight: 600,
    paddingLeft: '2px',
  },
}));

export default useTrainTooltipTitleStyles;
