import React, { useEffect, useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

import useTrainWordStyles from './TrainWordStyles';

import TrainTooltipTitle from '../TrainTooltip/TrainTooltipTitle';
import useTranslate from '../../../../../hooks/useTranslate';
import useWordMeaning from '../../../../../hooks/useWordMeaning';

const HtmlTooltip = withStyles((theme) => ({
  tooltip: {
    zIndex: '99999',
    display: 'block',
    backgroundColor: '#f5f5f9',
    color: 'rgba(0, 0, 0, 0.87)',
    maxWidth: 220,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    padding: '13px 17px 12px 17px',
  },
}))(Tooltip);

const TrainWord = (props) => {
  const { value, descriptionText } = props;
  const [word, setWord] = useState('');
  const translator = useTranslate(word);
  const wordMeaning = useWordMeaning(word);
  const classes = useTrainWordStyles();
  const [open, setOpen] = React.useState(false);

  const handleTooltipOpen = () => {
    setWord(value.toLowerCase());
    setOpen(true);
  };

  const handleTooltipClose = () => {
    setOpen(false);
  };

  return (
    <ClickAwayListener onClickAway={handleTooltipClose}>
      <HtmlTooltip
        title={(
          <>
            <TrainTooltipTitle
              text={value}
              translatedText={translator.translated}
              descriptionText={wordMeaning.wordInfo}
            />
          </>
        )}
        onClose={handleTooltipClose}
        open={open}
        interactive
        disableFocusListener
        disableHoverListener
        disableTouchListener
      >
        <Button className={`${classes.word} ${open && classes.clickedButton}`} onClick={handleTooltipOpen}>{value}</Button>
      </HtmlTooltip>
    </ClickAwayListener>
  );
};

export default TrainWord;
