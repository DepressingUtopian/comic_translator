import { makeStyles } from '@material-ui/core/styles';

const useTrainWordStyles = makeStyles((theme) => ({
  word: {
    minWidth: '0',
  },
  clickedButton: {
    background: 'aliceblue',
  },
}
));

export default useTrainWordStyles;
