import React, { useState } from 'react';
import { TextField } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';

import useTranslateTabStyles from './TranslateTabStyles';
import useTranslate from '../../../../hooks/useTranslate';

const TranslateTab = ({ text, onChange }) => {
  const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      '& > * + *': {
        marginLeft: theme.spacing(2),
      },
    },
  }));
  const circularClasses = useStyles();
  const classes = useTranslateTabStyles();
  const translatedResult = useTranslate(text, false).translated;
  return (
    <div>
      {translatedResult
        ? (
          <TextField
            className={classes.TextField}
            multiline
            value={translatedResult}
            onChange={(event) => onChange && onChange(event.target.value)}
            variant="outlined"
            rows={10}
          />
        ) : (
          <div className={circularClasses.root}>
            <CircularProgress />
          </div>
        ) }
    </div>

  );
};

export default TranslateTab;
