import { makeStyles } from '@material-ui/core/styles';

const useTranslateTabStyles = makeStyles((theme) => ({
  TextField: {
    width: '100%',
  },
}));

export default useTranslateTabStyles;
