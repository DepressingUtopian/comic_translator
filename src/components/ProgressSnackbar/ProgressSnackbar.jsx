import React, { useEffect } from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import CircularProgressWithLabel from '../CircularProgressWithLabel/CircularProgressWithLabel';

export default function ProgressSnackbar({ status, progressValue }) {
  const [open, setOpen] = React.useState(false);

  useEffect(() => {
    if (progressValue === 100) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  }, [progressValue]);
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}
      open={open}
      onClose={handleClose}
      message={status}
      autoHideDuration={progressValue === 1 ? 1500 : null}
      action={(
        <>
          <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
            <CloseIcon fontSize="small" />
          </IconButton>
          <CircularProgressWithLabel value={progressValue} />
        </>
        )}
    />
  );
}
