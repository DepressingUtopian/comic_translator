import React from 'react';
import ToggleButton from '@material-ui/lab/ToggleButton';
import Tooltip from '@material-ui/core/Tooltip';

export default function SelectButton(props) {
  const {
    onClick,
    isSelectedImage,
    icon,
    value,
    arialabel,
  } = props;

  function handleButtonClick() {
    onClick(!isSelectedImage);
  }

  return (

    <Tooltip title="Выделить область" placement="right">
      <ToggleButton
        value={value}
        aria-label={arialabel}
        selected={isSelectedImage}
        onClick={handleButtonClick}
      >
        {icon}
      </ToggleButton>
    </Tooltip>
  );
}
