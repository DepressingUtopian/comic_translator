import React, { useEffect, useRef } from 'react';

export default function CroppedCanvas(props) {
  const tempCanvasRef = useRef(null);
  const { setTempCanvas } = props;

  useEffect(() => {
    setTempCanvas(tempCanvasRef.current);
  }, []);

  return (
    <canvas style={{ border: '1px solid black', display: 'none' }} ref={tempCanvasRef} />
  );
}
