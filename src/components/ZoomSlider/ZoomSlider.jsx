import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Slider from '@material-ui/core/Slider';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme) => ({
  root: {
    height: 300,
    position: 'absolute',
    left: '-52px',
    padding: '12px 2px',
  },
}));
const marks = [
  {
    value: 1,
  },
  {
    value: 2,
  },
  {
    value: 3,
  },
  {
    value: 4,
  },
  {
    value: 5,
  },
  {
    value: 6,
  },
  {
    value: 7,
  },
  {
    value: 8,
  },
];

function valuetext(value) {
  return `${value}x`;
}

const ZoomSlider = ({
  onChange, className, value, min, max,
}) => {
  const classes = useStyles();
  return (
    <Paper className={`${classes.root} ${className}`}>
      <Slider
        orientation="vertical"
        getAriaValueText={valuetext}
        aria-labelledby="vertical-slider"
        value={value}
        step={0.05}
        min={1}
        max={8}
        valueLabelDisplay="auto"
        marks={marks}
        onChange={(event, _value) => onChange(_value)}
      />
    </Paper>
  );
};

export default ZoomSlider;
