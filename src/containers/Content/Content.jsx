import React from 'react';
import { Typography, Paper } from '@material-ui/core';
import useStylesFooter from '../Footer/FooterStyles';

const Content = () => {
  const footerStyles = useStylesFooter();

  return (
    <footer>
      <Paper className={footerStyles.paper}>
        <Typography variant="subtitle1">
          © 2021 Comic Translator. Все права защищены.
        </Typography>
      </Paper>
    </footer>
  );
};

export default Content;
