import { makeStyles } from '@material-ui/core/styles';

const useStylesFooter = makeStyles({
  paper: {
    minHeight: '66px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'rgba(0,0,0,0.5)',
    flex: '0 0 auto',
  },
});

export default useStylesFooter;
