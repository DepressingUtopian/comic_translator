import React, { useEffect } from 'react';
import { useHistory } from 'react-router';
import HelpIcon from '@material-ui/icons/Help';
import ArchiveComponent from '../../components/ArchiveComponent/ArchiveComponent';
import DialogTitle from '../../components/GuideDialog/GuideDialog';

export default function HomeContainer({ classes, dataBase, isComicsExist }) {
  const history = useHistory();

  useEffect(() => {
    if (isComicsExist) {
      history.push('/reader');
    }
  }, []);
  return (
    <div className={classes.wrapper}>
      <div className={classes.headerContainer}>
        <div className={classes.header}>ComicTranslator</div>
        <div className={classes.subHeader}>
          WEB-приложение для перевода комиксов
        </div>
      </div>
      <div className={classes.contentContainer}>
        <div className={classes.descriptionContainer} elevation={3}>
          <div className={classes.descriptionHeader}>Добро пожаловать, странник!</div>
          <div className={classes.descriptionContent}>
            Данное web-приложение разработано, для того чтобы помочь любителю комиксов,
            незнающему иностранный язык, перевести комикс и подучить язык.
            Если ничего не понятно, то помочь должен мини-гайд,  чтобы открыть его нажми:
            <HelpIcon fontSize="inherit" />
          </div>

        </div>
        <ArchiveComponent dataBase={dataBase} />
        <DialogTitle />
      </div>
    </div>

  );
}
