import React from 'react';
import {
  AppBar, Toolbar, IconButton, Typography,
} from '@material-ui/core';
import BookIcon from '@material-ui/icons/Book';

import useStylesMenu from './MenuStyles';

const Menu = () => {
  const menuStyles = useStylesMenu();
  return (
    <AppBar position="static" color="inherit" className={menuStyles.menu}>
      <Toolbar>
        <IconButton edge="start" color="inherit" aria-label="menu" href="./">
          <BookIcon />
        </IconButton>
        <Typography className={menuStyles.menuHeader}>
          ComicTranslator
        </Typography>
      </Toolbar>
    </AppBar>
  );
};

export default Menu;
