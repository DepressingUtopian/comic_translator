import { makeStyles } from '@material-ui/core/styles';

const useStylesMenu = makeStyles({
  menu: {
    minHeight: '64px',
  },
  menuHeader: {
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: '20px',
    lineHeight: '24px',
  },
});

export default useStylesMenu;
