import { makeStyles } from '@material-ui/core/styles';

const useStylesPageContent = makeStyles({
  container: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100%',
  },
});

export default useStylesPageContent;
