import React from 'react';
import PropTypes from 'prop-types';

import Menu from '../Menu/Menu';
import Footer from '../Footer/Footer';
import useStylesPageContent from './PageContentStyles';

const PageContentWrapper = (props) => {
  const pageContentStyles = useStylesPageContent();
  const { children, hideMenu, hideFooter } = props;
  return (

    <div className={pageContentStyles.container}>
      {!hideMenu ? <Menu /> : null }
      { children }
      {!hideFooter ? <Footer /> : null }
    </div>

  );
};

PageContentWrapper.propTypes = {
  children: PropTypes.element.isRequired,
};

export default PageContentWrapper;
