import React, { useState, useEffect } from 'react';
import useRecognize from '../../hooks/ocr/useRecognize';
import Popup from '../../components/Popup/Popup';
import ProgressSnackbar from '../../components/ProgressSnackbar/ProgressSnackbar';
import AlertSnackbar from '../../components/AlertSnackbar/AlertSnackbar';

/**
 *
 * @param {data} state - (url на картинку или base64)
 * @returns jsx разметка
 */
const RecognizeContent = ({ data, onVisiblePopup, isVisiblePopup }) => {
  const [status, setStatus] = useState(null);
  const [progressValue, setProgressValue] = useState(null);
  const [isOpenSnackbarAlert, setIsOpenSnackbarAlert] = useState(false);

  const [popupData, setPopupData] = useState({ content: [] });
  const [errorMessage, setErrorMessage] = useState(null);
  const [errorSeverity, setErrorSeverity] = useState('warning');
  const [recDataCounter, setRecDataCounter] = useState(0);

  const onProgress = (logObj) => {
    setStatus(logObj.status);
    setProgressValue(logObj.progress);
  };

  const onSummonAlert = (message, severity) => {
    setErrorMessage(message);
    setErrorSeverity(severity);
    setIsOpenSnackbarAlert(true);
  };

  const recData = useRecognize(data, onProgress);

  const onUpdateExternalData = (item) => {
    const targetItem = popupData.content.find((elem) => elem.id === item.id);
    targetItem.text = item.text;
  };

  const onDeleteFromExternalData = (item) => {
    setPopupData((prevState) => ({
      ...prevState,
      content: popupData.content.filter((elem) => elem !== item),
    }));
  };

  const onError = (message) => {
    onSummonAlert(message, 'error');
  };

  const onAlertClose = () => {
    setIsOpenSnackbarAlert(false);
  };

  const addPopupData = (obj) => {
    const { content } = popupData;
    content.push(obj);
    setPopupData({ content });
    setRecDataCounter((prev) => prev + 1);
  };

  const formatText = (text) => text.replace(/ +|(?= )/g, ' ');

  useEffect(() => {
    if (!recData) {
      return;
    }

    if (recData?.words[0]?.text.replace(/\n|\r|\0|\r|\t| /g, '') === '' || recData.confidence < 60) {
      onSummonAlert('Распознование текста прошло не удачно!', 'error');
      return;
    }
    onSummonAlert('Распознование текста прошло удачно!', 'success');
    addPopupData({
      id: recDataCounter,
      text: formatText(recData.text),
      header: recData.text.substring(0, 20),
    });
  }, [recData]);
  return (
    <>
      { isVisiblePopup
        ? (
          <Popup
            data={popupData}
            onUpdateExternalData={onUpdateExternalData}
            onDeleteFromExternalData={onDeleteFromExternalData}
            onError={onError}
            onVisible={onVisiblePopup}
          />
        ) : null}
      <ProgressSnackbar status={status} progressValue={progressValue} />
      <AlertSnackbar
        message={errorMessage}
        severity={errorSeverity}
        isOpen={isOpenSnackbarAlert}
        onAlertClose={onAlertClose}
      />
    </>
  );
};

export default RecognizeContent;
