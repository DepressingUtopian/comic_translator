/* eslint-disable max-len */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-shadow */
import React, { useEffect, useState, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';

import Paper from '@material-ui/core/Paper';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import ToggleButton from '@material-ui/lab/ToggleButton';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import CropFreeIcon from '@material-ui/icons/CropFree';
import CloseIcon from '@material-ui/icons/Close';
import LocationSearchingIcon from '@material-ui/icons/LocationSearching';
import FlipToBackIcon from '@material-ui/icons/FlipToBack';

import PaginationControlled from '../../components/PaginationControlled/PaginationControlled';
import CustomImage from '../../components/CustomImage/CustomImage';
import ImageWithSelecting from '../../components/ImageWithSelecting/ImageWithSelecting';
import SelectButton from '../../components/SelectButton/SelectButton';
import RecognizeContent from '../RecognizeContent/RecognizeContent';
import AlertSnackbar from '../../components/AlertSnackbar/AlertSnackbar';
import ZoomSlider from '../../components/ZoomSlider/ZoomSlider';

import useDatabase from '../../hooks/useDatabase';

const useStyles = makeStyles((theme) => ({
  backgroundSlider: {
    '&:before': {
      content: '""',
      position: 'absolute',
      top: '0',
      left: '0',
      width: '100%',
      height: '100%',
      backgroundImage: (props) => (props?.background ? `url(${props.background})` : ''),
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center center',
      backgroundSize: 'cover',
      filter: 'grayscale(25%) blur(1.4px) brightness(0.7)',
    },

  },
  zoomableContainer: {
    transition: theme.transitions.create(['height', 'width', 'scrollLeft', 'scrollTop'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    '&>img': {
      height: (props) => (props?.scale ? `calc(100% * ${props.scale})` : ''),
      transition: theme.transitions.create(['height', 'width'], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
  },
  hide: {
    display: 'none',
  },
  archiveName: {
    top: '-5.4vh',
    color: 'white',
    width: 'max-content',
    padding: '3px',
    zIndex: '5',
    position: 'absolute',
    background: 'black',
    fontFamily: 'Roboto',
  },
}));

const MAX_DIFFERENCE_ASPECT_RATIO = 0.04;
const MIN_ZOOM = 1;
const MAX_ZOOM = 8;

const loadImage = (url) => new Promise((resolve, reject) => {
  const img = new Image();
  img.addEventListener('load', () => resolve(img));
  img.addEventListener('error', (err) => reject(err));
  img.src = url;
});

export default function Slider(props) {
  const { styles } = props;

  const {
    start,
  } = props;
  const dataBase = useDatabase();
  const history = useHistory();
  const [slideNumber, setSliderState] = useState(start);
  const [imgLinks, setImgLinks] = useState([]);
  const [isSelectedImage, setSelectedImage] = useState(false);
  const [ImageState, setImage] = useState(null);
  const [backgroundImage, setBackgroundImage] = useState(null);
  const [CSSImageHeight, setCSSImageHeight] = useState(null);
  const [CSSImageWidth, setCSSImageWidth] = useState(null);
  const [croppedImageBase64Url, setUrl] = useState(''); // base64Url выделенного изображения
  const [view, setView] = React.useState([]);
  const [isOpenAlert, setIsOpenAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const [isDoublePageMode, setIsDoublePageMode] = useState(false);
  const [isVisiblePopup, setIsVisiblePopup] = useState(true);
  const [isDisabledPopupButton, setIsDisabledPopupButton] = useState(true);
  const [scale, setScale] = useState(1);
  const jssStyles = useStyles({ background: backgroundImage, scale });
  const refZoomableContainer = useRef(null);
  const refDualPageButton = useRef(null);
  const [posMouseDown, setPosMouseDown] = useState({
    top: 0, left: 0, x: 0, y: 0,
  });
  useEffect(() => {
    if (imgLinks[0]) {
      setBackgroundImage(imgLinks[0]);
    }
  }, [imgLinks]);
  const onKeyDown = (e) => {
    switch (e.code) {
      case 'KeyQ':
        setScale(scale < MAX_ZOOM ? scale + 0.05 : scale);
        break;
      case 'KeyE':
        setScale(scale > MIN_ZOOM ? scale - 0.05 : scale);
        break;
      case 'KeyW':
        refZoomableContainer.current.scrollTop -= 2 * scale;
        break;
      case 'KeyS':
        refZoomableContainer.current.scrollTop += 2 * scale;
        break;
      case 'KeyA':
        refZoomableContainer.current.scrollLeft -= 2 * scale;
        break;
      case 'KeyD':
        refZoomableContainer.current.scrollLeft += 2 * scale;
        break;
      case 'Escape':
        setScale(MIN_ZOOM);
        break;
      case 'ArrowLeft':
        if (slideNumber !== 1) {
          setSliderState(slideNumber - 1);
        }
        break;
      case 'ArrowRight':
        if (slideNumber !== imgLinks.length) {
          setSliderState(slideNumber + 1);
        }
        break;
      case 'KeyU':
        refDualPageButton.current.click();
        break;
      default:
        break;
    }
  };

  const onAlertClose = () => {
    setIsOpenAlert(false);
  };

  const toggleDoublePageMode = () => {
    setIsDoublePageMode(!isDoublePageMode);
  };

  useEffect(() => {
    dataBase.onsuccess = () => {
      const objectStore = dataBase.result.transaction(['comic']).objectStore('comic');
      objectStore.getAll().onsuccess = (event) => {
        setImgLinks(event.target.result.map((file) => URL.createObjectURL(file)));
      };
    };
  }, []);

  useEffect(() => {
    let currentIsDoublePageMode = isDoublePageMode;
    if (!isDoublePageMode && view.includes('double-page')) {
      setIsDoublePageMode(true);
      currentIsDoublePageMode = true;
    }

    if (currentIsDoublePageMode && slideNumber < imgLinks.length) {
      Promise.all([
        loadImage(imgLinks[slideNumber - 1]),
        loadImage(imgLinks[slideNumber]),
      ]).then(([img1, img2]) => {
        const aspectRatioImg1 = img1.width / img1.height;
        const aspectRatioImg2 = img2.width / img2.height;

        if (MAX_DIFFERENCE_ASPECT_RATIO < Math.abs(aspectRatioImg1 - aspectRatioImg2)) {
          setIsDoublePageMode(false);
          setAlertMessage('Картинка не может быть отображена в двухстраничном режиме!');
          setIsOpenAlert(true);
        }
      }).catch((err) => console.error(err));
    }
  }, [slideNumber, view]);

  const SelectAnotherComics = () => {
    history.push('/');
  };

  const handleChange = (event, nextView) => {
    setView(nextView);
  };

  const onVisiblePopup = (isVisible) => {
    setIsDisabledPopupButton(!isVisible && isVisiblePopup);
  };

  const onChangeZoomSlider = (value) => {
    setScale(value || scale);
  };

  const mouseDownHandler = (event) => {
    const { target } = event;
    if (!isSelectedImage) {
      const currentTarget = target.localName === 'img' ? target.offsetParent : target;
      setPosMouseDown({
        left: currentTarget.scrollLeft,
        top: currentTarget.scrollTop,
        x: event.clientX,
        y: event.clientY,
      });
      target.style.cursor = 'grabbing';
      target.style.userSelect = 'none';
    } else {
      target.style.cursor = 'crosshair';
    }
  };

  const mouseUpHandler = (event) => {
    const { target } = event;
    if (!isSelectedImage) {
      target.style.cursor = 'grab';
      target.style.removeProperty('user-select');
    } else {
      target.style.cursor = 'default';
    }
  };

  const mouseMoveHandler = (event) => {
    const { target } = event;
    const currentTarget = target.localName === 'img' ? target.offsetParent : target;
    if (target.style.userSelect) {
      const dx = event.clientX - posMouseDown.x;
      const dy = event.clientY - posMouseDown.y;
      currentTarget.scrollTop = posMouseDown.top - dy * scale;
      currentTarget.scrollLeft = posMouseDown.left - dx * scale;
    }
  };

  useEffect(() => {
    setIsVisiblePopup(!view.includes('hide-popup'));
  }, [view]);

  const imagesContainer = isSelectedImage ? (
    <>
      <ImageWithSelecting
        theCSSImageWidth={CSSImageWidth}
        theCSSImageHeight={CSSImageHeight}
        source={imgLinks[slideNumber - 1]}
        croppedImageBase64Url={croppedImageBase64Url}
        setUrl={setUrl}
      />
      {isDoublePageMode && imgLinks.length > slideNumber
        ? (
          <ImageWithSelecting
            theCSSImageWidth={CSSImageWidth}
            theCSSImageHeight={CSSImageHeight}
            source={imgLinks[slideNumber]}
            croppedImageBase64Url={croppedImageBase64Url}
            setUrl={setUrl}
          />
        ) : null}

    </>
  ) : (
    <>
      <CustomImage
        alt="sd"
        setImage={setImage}
        imageSource={imgLinks[slideNumber - 1]}
        setCSSImageWidth={setCSSImageWidth}
        setCSSImageHeight={setCSSImageHeight}
      />
      {isDoublePageMode && imgLinks.length > slideNumber
        ? (
          <CustomImage
            alt="empty_image"
            setImage={setImage}
            imageSource={imgLinks[slideNumber]}
            min={MIN_ZOOM}
            max={MAX_ZOOM}
            setCSSImageWidth={setCSSImageWidth}
            setCSSImageHeight={setCSSImageHeight}
          />
        ) : null}
    </>
  );

  return (
    <div className={`${styles.content} ${jssStyles.backgroundSlider}`}>
      {
        croppedImageBase64Url
          // eslint-disable-next-line max-len
          ? <RecognizeContent data={croppedImageBase64Url} isVisiblePopup={isVisiblePopup} onVisiblePopup={onVisiblePopup} /> : null
      }

      {/* eslint-disable-next-line max-len */}
      {/* eslint-disable-next-line jsx-a11y/no-static-element-interactions,jsx-a11y/no-noninteractive-tabindex */}
      <div className={styles.wrapper} tabIndex={0} onKeyDown={onKeyDown}>

        <div className={styles.slider}>
          <div className={jssStyles.archiveName}>
            {localStorage.getItem('archiveName')}
          </div>
          <div
            className={`${styles.imageContainer} ${jssStyles.zoomableContainer}`}
            ref={refZoomableContainer}
            onMouseMove={mouseMoveHandler}
            onMouseDown={mouseDownHandler}
            onMouseUp={mouseUpHandler}
          >
            {imagesContainer}
          </div>
          <Paper className={styles.buttonsContainer}>
            <ToggleButtonGroup orientation="vertical" value={view} onChange={handleChange}>
              <Tooltip title="Приблизить/Отдалить" placement="right">
                <ToggleButton value="zoom" aria-label="zoom" disabled={isSelectedImage}>
                  <LocationSearchingIcon />
                </ToggleButton>
              </Tooltip>
              <SelectButton
                onClick={setSelectedImage}
                setCSSImageWidth={setCSSImageWidth}
                setCSSImageHeight={setCSSImageHeight}
                ImageState={ImageState}
                isSelectedImage={isSelectedImage}
                icon={<CropFreeIcon />}
                value="highlight"
                aria-label="highlight"
              />
              <Tooltip title="Двустраничный режим" placement="right">
                <ToggleButton value="double-page" aria-label="double-page" onClick={() => toggleDoublePageMode()}>
                  <MenuBookIcon />
                </ToggleButton>
              </Tooltip>
              <Tooltip title="Показать/Скрыть pop-up" placement="right">
                <ToggleButton value="hide-popup" aria-label="hide-popup" selected={isVisiblePopup && !isDisabledPopupButton} disabled={isDisabledPopupButton}>
                  <FlipToBackIcon />
                </ToggleButton>
              </Tooltip>
              <Tooltip title="Выбрать другой комикс" placement="right">
                <ToggleButton value="exit" aria-label="exit" onClick={SelectAnotherComics}>
                  <CloseIcon />
                </ToggleButton>
              </Tooltip>
            </ToggleButtonGroup>
          </Paper>
          <ZoomSlider
            className={(view.includes('zoom') && !isSelectedImage) ? null : jssStyles.hide}
            onChange={onChangeZoomSlider}
            value={scale}
          />
          <PaginationControlled
            isSelectedImage={isSelectedImage}
            imgLinks={imgLinks}
            setSliderState={setSliderState}
            slideNumber={slideNumber}
          />
        </div>
        <AlertSnackbar message={alertMessage} isOpen={isOpenAlert} severity="warning" onAlertClose={onAlertClose} />
      </div>

    </div>
  );
}
