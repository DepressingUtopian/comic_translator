import { createWorker, PSM } from 'tesseract.js';
import { useEffect, useState } from 'react';
// Можно добавить поддержку нескольких языков в будущем
const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890,.'-?! ";

/**
 * Hook для взаимодейсвия с Tesseract OCR
 * @param {string} data - url или данные в виде base64, если state,
 * тогда при его изменении Tesseract обновит данные в recData
 * @param {function()} onProgress - callback для получения логов работы Tesseract OCR
 * @returns recData - state-объект представляющий результат работы Tesseract
 */
const useRecognize = (data, onProgress) => {
  const [recData, setRecData] = useState(null);
  const [worker, setWorker] = useState(null);
  const [isWorkerInit, setIsWorkerInit] = useState(false);
  const [isRecognizing, setIsRecongizing] = useState(false);
  async function initWorker() {
    const options = {
      tessedit_char_whitelist: alphabet,
      tessedit_pageseg_mode: PSM.AUTO,
      tessjs_create_hocr: '0',
      tessjs_create_tsv: '0',

    };
    await worker.load();
    await worker.loadLanguage('eng');
    await worker.initialize('eng');
    await worker.setParameters(options);
    setIsWorkerInit(true);
  }

  async function getRecData(_data) {
    setIsRecongizing(true);
    return worker.recognize(_data).then((recObj) => {
      setIsRecongizing(false);
      return recObj.data;
    });
  }

  useEffect(() => {
    async function create() {
      setWorker(createWorker({ logger: (m) => onProgress && onProgress(m) }));
      return () => {
        worker.terminate();
        setIsWorkerInit(false);
      };
    }
    create();
  }, []);

  useEffect(() => {
    async function init() {
      if (worker === null || isWorkerInit) {
        return;
      }
      await initWorker();
      setWorker(worker);
    }
    init();
  }, [worker]);

  useEffect(() => {
    async function update() {
      if (!isWorkerInit) {
        return;
      }
      if (!isRecognizing) {
        setRecData(await getRecData(data)); // вот тут было data[0] но так почему-то не работает
      }
    }
    update();
  }, [data, isWorkerInit]);

  return recData;
};

export default useRecognize;
