// import { useState } from 'react';

const useDatabase = () => {
  // const [dataBase, setDataBase] = useState(null);
  const connection = window.indexedDB.open('books', 1);

  connection.onupgradeneeded = () => {
    connection.result.createObjectStore('comic', {
      keyPath: 'id',
      autoIncrement: true,
    });
  };
  // connection.onsuccess = () => connection;
  connection.onerror = (e) => console.error('Error loading database', e);
  // return { dataBase };
  return connection;
};

export default useDatabase;
