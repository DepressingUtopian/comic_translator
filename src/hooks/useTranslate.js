import { useState, useEffect } from 'react';
/**
 * Hook для получения перевода слова
 * @param {string} text - входящий текст, предполагается что в вызывающем компоненте
 * есть state в котором лежит текущее значение hover
 * @param {function()} debounced - обёрнутое в debounce обращение к API
 * @returns {string|array} translated - в зависимости от типа api возвращает либо строку
 * либо массив значений(в текущей редакции не реализовано)
 */

async function getTranslation(text) {
  const params = new URLSearchParams({
    lang: 'ru',
    text,
  });
  const url = `https://just-translated.p.rapidapi.com/?${params}`;
  const data = await fetch(url, {
    method: 'GET',
    headers: {
      'x-rapidapi-key': '8c4cec8f5bmsh09f6b00415fa3acp17e3d0jsn25e14c158d44',
      'x-rapidapi-host': 'just-translated.p.rapidapi.com',
    },
  });
  const TranslationObject = await data.json();
  return TranslationObject.text;
}

const useTranslate = (text) => {
  const [translated, setTranslated] = useState(null);

  useEffect(() => {
    if (text.lenght !== 0) {
      getTranslation(text).then((translatedText) => setTranslated(translatedText));
    }
  }, [text]);

  return { translated };
};
export default useTranslate;
