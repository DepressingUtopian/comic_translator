import { useState, useEffect } from 'react';

async function getSense(word) {
  const data = await fetch(`https://lingua-robot.p.rapidapi.com/language/v1/entries/en/${word}`, {
    method: 'GET',
    headers: {
      'x-rapidapi-key': '8c4cec8f5bmsh09f6b00415fa3acp17e3d0jsn25e14c158d44',
      'x-rapidapi-host': 'lingua-robot.p.rapidapi.com',
    },
  });
  const definitionObject = await data.json();
  if (definitionObject.entries[0]) {
    return definitionObject.entries[0].lexemes[0].senses[0].definition;
  }
  return ('definition was not found');
}

const useWordMeaning = (word) => {
  const [wordInfo, setWordInfo] = useState(word);

  useEffect(() => {
    if (word) {
      getSense(word).then((definiton) => { setWordInfo(definiton); });
    }
  }, [word]);

  return { wordInfo };
};

export default useWordMeaning;
