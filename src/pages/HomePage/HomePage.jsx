import React from 'react';
import HomePageStyles from './HomePageStyles';
import HomeContainer from '../../containers/HomeContainer/HomeContainer';

export default function HomePage({ isComicsExist, dataBase }) {
  const classes = HomePageStyles();
  return (
    <HomeContainer classes={classes} dataBase={dataBase} isComicsExist={isComicsExist} />
  );
}
