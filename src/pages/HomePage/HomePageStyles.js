import { makeStyles } from '@material-ui/core/styles';

import bg from '../../assets/images/bg.jpg';

const homePageStyles = makeStyles({

  wrapper: {
    flex: '1 0 auto',
    height: '100%',
    color: 'white',
    '&:before': {
      content: '""',
      position: 'absolute',
      top: '0',
      left: '0',
      width: '100%',
      height: '100%',
      backgroundImage: `url(${bg})`,
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center center',
      backgroundSize: 'cover',
      filter: 'grayscale(25%) brightness(0.4)',
    },
  },
  mainConainer: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    alignItems: 'center',
  },
  headerContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: '6.25rem',
    marginBottom: '10rem',
    zIndex: '2',
    position: 'relative',
    '& svg': { color: 'white', fontSize: '9.1875rem' },
  },
  header: {
    fontFamily: 'Neucha',
    fontSize: '5rem',
    background: 'linear-gradient(#9fff74, #2d3de8)',
    ' -webkit-background-clip': 'text',
    '-webkit-text-fill-color': 'transparent',
  },
  subHeader: {
    color: 'white',
    fontFamily: 'Rubik',
    fontSize: '2rem',
  },
  titleContainer: {
    zIndex: '2',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
  },
  contentContainer: {
    width: '100%',
    fontWeight: '300',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    zIndex: '2',
    position: 'relative',
  },
  descriptionContainer: {
    maxWidth: '40vw',
    padding: '1vw',
    background: 'rgba(0, 0, 0, 0.74)',
    boxShadow: '0px 3px 3px -2px rgb(0 0 0 / 20%), 0px 3px 4px 0px rgb(0 0 0 / 14%), 0px 1px 8px 0px rgb(0 0 0 / 12%)',
    borderRadius: '4.2px',
    height: 'fit-content',
  },
  descriptionHeader: {
    fontFamily: 'RobotoSlab',
    fontSize: '2rem',
    marginBottom: '0.8rem',
    color: '#0ba5ff',
  },
  descriptionContent: {
    fontFamily: 'Rubik',
    fontSize: '1.5rem',
    color: 'white',
    textAlign: 'justify',
    '& svg': {
      verticalAlign: 'middle',
    },
  },
  text: {
    // fontSize: '1.125rem',
    // lineHeight: '1.5rem',

    maxWidth: '20vw',
  },
  dragndrop: {
    border: 'none',
    color: 'black',
    maxWidth: '20vw',
  },
  '@media (max-width: 1605px)': {
    headerContainer: {
      marginTop: '2rem',
      marginBottom: '4rem',
      '& h1': {
        fontSize: '32px',
      },
      '& svg': {
        fontSize: '4.1875rem',
      },
    },
    contentContainer: {
      fontWeight: 200,
      '& h6': {
        fontSize: '16px',
      },

      ' & p': {
        fontSize: '16px',
      },
    },
  },
});
export default homePageStyles;
