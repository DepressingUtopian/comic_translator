import React from 'react';
import styles from '../../assets/styles/slider.module.css';
import Slider from '../../containers/Slider/Slider';

export default function ReaderPage({ dataBase }) {
  return (
    <Slider styles={styles} start={1} dataBase={dataBase} imageExt="jpg" />
  );
}
